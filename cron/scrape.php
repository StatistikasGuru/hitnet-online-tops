<?php
    require __DIR__ . '/../config.php';
    require __DIR__ . '/../db.php';

    $url = 'https://m.hitnet.lv/?id=1&do=1';


    $dom = new DOMDocument();

    libxml_use_internal_errors(true);

    $loaded = $dom->loadHTMLFile($url);

    $textContent = $dom->textContent;

    if (!$loaded) {
        throw new Exception('Cannot get the content');
    }

    $xpath = new DOMXPath( $dom );

    $list = $xpath->query( '//div[@class="list_list"]//a' );

    $users = [];

    foreach ($list as $link) {
        
        $linkDom = new DOMDocument;
        $linkDom->appendChild($linkDom->importNode($link,true));
        $xpath = new DOMXPath( $linkDom );

        $title = trim($xpath->query("//span")->item(0)->nodeValue);
        $place = $xpath->query("//span/following-sibling::text()[1]")->item(0)->nodeValue;

        $place = htmlentities($place, null, 'utf-8');
        $place = trim(str_replace(['&nbsp;', '-'], "", $place));
        $place = html_entity_decode($place);

        $users[] = ['user' => $title, 'place' => $place];
    }

    if(!empty($users)) {
        $db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

        $sql = "INSERT IGNORE INTO days
                (date, username, place, seconds) VALUES ";

        $rows = [];
        foreach ($users as $u) {
            $rows[] = "(CURDATE(), '" . $u['user'] . "', '" . $u['place'] . "', " . DATA_SCRAPE_INTERVAL . ")";
        }
        
        $sql .= implode(',', $rows);

        $sql .= " ON DUPLICATE KEY UPDATE seconds=seconds + " . DATA_SCRAPE_INTERVAL;

        $res = $db->query($sql);

        if(!$res) {
            $error = mysqli_error($db);
            die($error);
        }
    }

    echo 'OK' . PHP_EOL;