<?php
require 'db.php';

$data = ['data' => []];

$sql = "SELECT
            (SELECT MAX(seconds) FROM days WHERE DATE(date) = CURDATE()) AS record,
            d.username,
            (SELECT SUM(seconds) FROM days ds WHERE ds.username=d.username AND DATE(date) = CURDATE()) AS user_seconds,
            d.place,
            DENSE_RANK() OVER (ORDER BY user_seconds DESC) AS dense_rank,
            UNIX_TIMESTAMP(last_seen) AS last_seen
        FROM days d
        WHERE DATE(d.date) = CURDATE()
        GROUP BY d.username
        ORDER BY user_seconds DESC
        LIMIT 100";

$rows = $db->query($sql);

if(!$rows) {
    $error = mysqli_error($db);
    die($error);
}

while($r = mysqli_fetch_assoc($rows)) {
    $time = gmdate('H:i', $r['user_seconds']);

    $is8 = (int) date('H') > 8;

    $medalRank = $is8
        ? '<i class="fas fa-medal rank rank-i-' . $r['dense_rank'] . ' rank-' . $r['dense_rank'] . '"></i> '
        : '';

    $user = $is8 && $r['dense_rank'] < 4
        ? '<span class="rank-' . $r['dense_rank'] . '">' . $r['username'] . '</span>'
        : $r['username'];

    $state = time() - $r['last_seen'] <= 100 ? 'On' : 'Off';

    $statusColor = 'On' === $state ? 'green' : 'red';


    $onlineStatus = '<i class="fas fa-power-off" style="font-size: x-small;color: ' . $statusColor .
                        '"></i><span style="display: none">' . $state . '</span>';

    $rank = $is8 && $r['dense_rank'] < 4
        ? $medalRank . '<span class="rank-' . $r['dense_rank'] . '">' . $r['dense_rank'] . '.</span>'
        : $r['dense_rank'] . '.';

    $atkariba = round(((int) $r['user_seconds'] / (int) $r['record']) * 100, 1);

    $atkariba = $atkariba > 100 ? '<span class="red">' . $atkariba . '%</span>' : "$atkariba%";

    $h24 = round(($r['user_seconds'] / 86400) * 100, 1);

    $h24 = $h24 > 50 ? '<span class="red">' . $h24 . '%</span>' : "$h24%";

    $data['data'][] =
        [
            $rank,
            $user,
            $onlineStatus,
            $time,
            //$r['place'],
            $atkariba,
            $h24
        ];
}


echo json_encode($data);