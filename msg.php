<?php

require 'db.php';

$redirectUrl = "index.php";

if(empty($_POST['msg'])) {
    header("Location: $redirectUrl");
    die();
}

$msg = trim($_POST['msg']);
$user = $_POST['username'] ?? 'hidden';
$hash = md5($user . $msg);

$sql = "INSERT IGNORE INTO messages
                SET user='" . mysqli_real_escape_string($db, $user) . "', msg='" . mysqli_real_escape_string($db, $msg) . "'";
$res = $db->query($sql);

if(!$res) {
    $error = mysqli_error($db);
    die($error);
} else {
    $redirectUrl .= '?sent=true';
}

header("Location: $redirectUrl");
