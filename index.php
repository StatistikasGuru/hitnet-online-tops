<?php
    require 'config.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <?php $dateToday = date('d.m.Y'); ?>
  <meta charset="utf-8">
  <title>Hitnet online tops <?= $dateToday ?></title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/add.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/fh-3.1.4/rg-1.1.0/sc-2.0.0/datatables.min.css"/>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <script src="https://cdn.datatables.net/v/dt/jq-3.3.1/dt-1.10.18/fh-3.1.4/rg-1.1.0/sc-2.0.0/datatables.min.js"></script>
    <link rel="icon" type="image/png" href="images/favicon.png">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="eleven columns center" style="margin-top: 10pt">
          <?php if(isset($_GET['sent'])): ?>
            <p class="notify">Ziņa tika nosūtīta!</p>
          <?php endif; ?>
        <h5 class="bottom-0">Hitnet online tops: <i><?= $dateToday ?></i></h5>
          <p class="bottom-0"><i>Atjaunojas reizi <?= TOP_UPDATE_INTERVAL / 1000?> sekundēs</i></p>
        <table id="table_id" class="display compact order-column" style="width:100%">
          <thead>
          <tr>
            <th>Vieta</th>
            <th>Niks</th>
            <th>On</th>
            <th>Stundas</th>
            <th>Atkarība</th>
            <th>24h</th>
          </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
      <div class="row">
          <hr/>
          <h5>Sensitīvie jautājumi un atbildes</h5>
          <?php
          $faq = [
              'Kas tas ira?!' => 'Iespējams, labākais <a href="https://m.hitnet.lv/">Hitnet čata</a> pavadītā laika uzskaites tops pasaulē.',
              'Ar kādu domu taisīts!?' => 'Gūt var ņemot, gūt var dodot, dodot gūtais neatņemams! /J. Pliekšāns & hitnetieši /',
              'Kas par šito samaksāja???' => 'Neviens. Projekts ir nekomerciāls un kopā ar godalgoto vietu ieguvējiem simbolizē trīs krietna hitnetieša vērtības - haļavu, radošo eksistenciālismu un neierobežotu brīvo laiku.',
              'Redzu sevi topā! Mans konts ir uzhakots!?' => 'Visdrīzāk, nē.',
              'Kas nosaka topā iegūto vietu?' => 'Šodien onlainā pavadītais laiks no 00:00 līdz 23:59',
              'Kā tas notiek???' => 'Rezultāti atjaunojas 24 stundas diennaktī, reizi minūtē pārbaudot onlaina sarakstu. Fiksācijas brīdī katram tiek pieskaitītas 60 sekundes.',
              'Kā tiek aprēķināta atkarība?' => 'Par etalonu pieņemot 1. vietas rezultātu(s) saskaņā ar formulu:  "nika pavadītais laiks / 1. vietas laiks x 100"',
              'Ko nozīmē aile: "24h"?' => 'Cik procentu no diennakts 24 stundām ir pavadīti onlainā',
              'Nedevu atļauju mani stalkot! Varu iesūdzēt tiesā?!' => 'Nē.',
              'Atdosi projektu man!? Piektdien nauda, parādā nepalikšu!!!' => 'Ok. Paņem viņu <a href="https://gitlab.com/StatistikasGuru/hitnet-online-tops">šeit</a>',
          ]; ?>
          <ul style="list-style-type: none">
              <?php
              foreach ($faq as $title => $info): ?>
                  <li>
                      # <b><?= $title ?></b>
                      <ul style="list-style-type: none">
                          <li>
                              <p><?= $info ?></p>
                          </li>
                      </ul>
                  </li>
              <?php
              endforeach;
              ?>
          </ul>
          <p class="center"><button id="talk-button">Man ir,ko teikt!!!</button></p>
      </div>
      <div id="msg-wrapper" class="row" style="display:none;">
          <form action="msg.php" method="post">
              <label for="username">Tavs Hitneta niks vai epasts</label>
              <input type="text" placeholder="Ieraksti to šeit" id="username" name="username"/>
              <label for="msg">Ko teiksi?</label>
              <textarea class="u-full-width" placeholder="Ieraksti jēgpilno vēstījumu šeit" id="msg" name="msg"></textarea>
              <p><i>Varbūtība saņemt atbildi ir aptuveni vienāda ar Kiviča kļūšanu par atturībnieku.</i></p>
              <input class="button-primary" type="submit" value="Izteikties">
          </form>
      </div>
      <div class="row copyright">
          <small><i>&#9400; 2019 by StatistikasGuru</i></small>
      </div>
  </div>
<script>
  $(document).ready( function () {
    var table  = $('#table_id').DataTable(
            {
              "ajax": 'data.php',
              "order": [[ 0, "asc" ]],
              "lengthChange": false,
              "pageLength": 100,
              "paging": false,
              "language": {
                  url: 'LV.json'
              }
            }
    );
    setInterval( function () {
      table.ajax.reload();
    }, <?= TOP_UPDATE_INTERVAL ?> );

    $("#talk-button").on('click', function () {
        $('html,body').animate({ scrollTop: 9999 }, 'slow');
        $("#msg-wrapper").show();
    })
  });
</script>
</body>
</html>
